# trivago Recruiting: PHP and JavaScript Case Study
This is a test project within trivago's technical recruiting process.

In doubt of technical issues you can send a mail with your questions.

## PHP Tasks: Requirements

### Technical
You need at least:

* PHP 5.3 or higher
* A text editor (vim, nano, sublime text, notepad++) or IDE (PHPStorm, Eclipse PDT, Netbeans) of your choice
* Some kind of shell, if you want to write unit tests. (Linux, OS X, *nix or Windows - be sure to check the executable files for windows compatibility)
* Composer if you want to write any tests for your code. See [this documentation to get started with composer](http://getcomposer.org/doc/00-intro.md#installation)

### Knowledge
You will be tested within the following areas of PHP development:

* Basic understanding of PHP's OOP implementation, including interfaces and design patterns.
* Namespaces, Closures/Anonymous functions
* Reading resources from a local file system location
* Coping with JSON as data format

## The tasks
With this code you will receive a number of tasks to resolve. Each task should
take between 15 and 60 minutes working time.

### Implement a basic PartnerService

Implement the interface for the Partner Service. Please use the JSON file in the data directory as your datasource. 
Your implementation must read the resultset from the datasource and pass the values from the json file into the corresponding classes from
the Entity namespace. 

The entities encapsulate each other:

(Hotel) -[hasMany]-> (Partner) -[hasMany]-> (Price)

The JSON file has a similar but not equal structure. Please take a deep look at both structures

### Build a basic validator for the Partner Entity

Your task is to build a validation mechanism for the Partner Entity's homepage
property. Place the validation class in a proper position within the given
architecture and ensure the value is valid. It is up to you how you implement
it and when to call it within the application's flow.

### Implement a way to get different implementations of the HotelServiceInterface

You can accomplish this by using a simple factory or abstract factory pattern or you can choose the more complex 
variant using dependency injection via inversion of control. Please choose the variant you feel most comfortable with, not
the one that you think might of bringing the most kudos. You might want to write a second implementation for the HotelServiceInterface, but
you don't have to. If you need one, you can think of a "PriceOrderedHotelService" or a "PartnerNameOrderedHotelService", which sort their 
results after receiving it from the partner service.



## JavaScript Tasks: Requirements

### Technical
You need at least:
* A Web browser that's not entirely outdated. Open index.html in the browser and click around.
  You will quickly realize that some things are "kind of" working whereas others are mere click
  dummies.

### Knowledge
You will be tested within the following areas of JS development:

* Basic jQuery knowledge
* Event handling
* Information hiding
* JavaScript code organization


## The tasks
With this code you will receive a number of tasks to resolve. Each task should
take you about 15 to 45 minutes to complete, but that's only a rough estimate.
Some of the tasks might be a little intertwined, so you'll solve part of one
task while solving another one. Usually, we'll see what was your intention, so
don't worry about such things.

### Improve main.js

The file main.js is written in poor style and contains errors. Please improve it. Particularly, have a look at
slideout-close-error.png, which illustrates the following bug:

Current behaviour:
* For any hotel, click on the "Map" button => The map slideout should open
* Now click on the "Info" button => the slideout content is updated, but the slideout closes

Desired behaviour:
* For any hotel, click on the "Map" button => The map slideout should open
* Now click on the "Info" button => the slideout content is updated, and the slideout stays open
* Click on the "Info" button again => the slideout closes (since the info slideout is already open)
* Adapt the logging accordingly

### Logger
There is a simple logger defined in Logger.js which counts certain user events of interest. It has
just one public variable called "events". This is unfortunate, since everyone can change or delete
the variable value. Please come up with a better solution that shields the list of logged events from
uncontrolled access, yet provides ways to read out information so that the logging overview in the
top right corner can still be implemented.

### Application structure
Have a look at the event handler for the click event on .js_slideout_trigger. Apparently, other program
components (the logger and the hotel history) are interested in slideout open/close events. However, it
should not be this event handler's job to update the right column.

Find a better way for different program components to communicate. Think of patterns like Mediator,
Observer, or use an event/message bus. This way you can maybe also get rid of the terrible window.setInterval()
in displayLog.js.


