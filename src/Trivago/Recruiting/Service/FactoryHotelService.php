<?php

namespace Trivago\Recruiting\Service;
/**
 *	Factory class for all kind of hotel services. 
 *
 * @author pulkit
 */

	
class FactoryHotelService
{

	/**
	 * Create an instance for service depending upon incomming user request.
	 *  
	 * @var array
	 */
	public static function create($type)
	{

		switch ($type) {
			case 'Unordered':
	   			return new UnorderedHotelService(new PartnerService);
				break;
			case 'HotelNameOrdered':
				return new NameOrderedHotelService(new PartnerService);
				break;		
			default:
				# code...
				break;
		}
		
	}


   
}