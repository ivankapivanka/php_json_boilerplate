<?php
namespace Trivago\Recruiting\Service;

/**
 * This class is an (unfinished) example implementation of an unordered hotel service.
 *
 * @author mmueller
 */
class UnorderedHotelService extends AbstractHotelService
{


    /**
     * @abstract method implemented
     */
    public function getHotelsForCity($sCityName)
    {
        if (!isset($this->aCityToIdMapping[$sCityName]))
        {
            throw new \InvalidArgumentException(sprintf('Given city name [%s] is not mapped.', $sCityName));
        }

        $iCityId = $this->aCityToIdMapping[$sCityName];
        $aPartnerResults = $this->oPartnerService->getResultForCityId($iCityId);
        return $aPartnerResults;
    }
}

