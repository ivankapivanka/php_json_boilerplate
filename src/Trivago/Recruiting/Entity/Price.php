<?php

namespace Trivago\Recruiting\Entity;

/**
 * Represents a single price from a search result
 * related to a single partner.
 * 
 * @author mmueller
 */
class Price
{
    /**
     * Description text for the rate/price
     * 
     * @var string
     */
    public $sDescription;

    /**
     * Price in euro
     * 
     * @var float
     */
    public $fAmount;

    /**
     * Arrival date, represented by a DateTime obj
     * which needs to be converted from a string on 
     * write of the property.
     *
     * @var \DateTime
     */
    private $oFromDate;

    /**
     * Departure date, represented by a DateTime obj
     * which needs to be converted from a string on 
     * write of the property
     *
     * @var \DateTime
     */
    private $oToDate;


    /**
     * @param $date string with format YYYY-MM-DD
     *
     * 
     * Converting code from string to data object
     */
    public function getDateObj($date){
        $dateTime = \datetime::createfromformat('Y-m-d',$date);
        return $dateTime;
    }

    /**
     * @param $date string
     *
     * Setter for oToDate
     */
    public function setToDate($date){
        $this->oToDate = $this->getDateObj($date);
    }

    /**
     * @param $date string
     *
     * Setter for oFromDate
     */
    public function setFromDate($date){
        $this->oFromDate = $this->getDateObj($date);
    }


     /**
     * @param $date string
     *
     * Getter for oToDate
     * @return datetime object for ToDate
     */
    public function getToDate($date){
        return $this->oToDate;
    }

    /**
     * @param $date string
     *
     * Getter for oFromDate
     * @return datetime object for FromDate
     */
    public function getFromDate($date){
        return $this->oFromDate;
    }



    /**
     * @param $desc, $amount, $from , $to
     *
     * Constructor
     */
    public function __construct($desc, $amount, $from, $to){

        $this->sDescription = $desc;
        $this->fAmount = $amount;
        
        $this->setToDate($to);
        $this->setFromDate($from);
        
    }

}
