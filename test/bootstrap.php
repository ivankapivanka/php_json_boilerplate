<?php
require __DIR__  . '/../src/SplClassLoader.php';

$oClassLoader = new \SplClassLoader('Trivago', __DIR__ . '/../src');
$oClassLoader->register();
