    /**
 * Trivago recruiting
 */



//Custom version of standard observer pattern

var logMessageCenter = new(function(){
//will carry functions for now, normally it takes the objects. For that current structure of code will get hurt.
    var entities = []; 

    return {

        register: function(observer){
            entities.push(observer);
            //anybody.prototype = this; // In case objects are the oberserver
        },

        changeNotify: function(data){
            //console.log("recieved logger"+ data);
            //transmiting data to all observer
            for(var j = 0; j< entities.length; j++){
                entities[j](data);
            }
        }

    }
})();

//register the view updater to listen to this

logMessageCenter.register(upDateLogView);

   
(function(){

    //protected from the window scope, Hence the logger variable is controled from unwanted access.
    var logger = new CountingLogger();
    var slideoutViewCount = 0;
    var oldType = "";


    /*JQUERY events can be encapsulated into a function of an object. 
    But dont know the whole structure of the remaining app, hence can't decide.

    Normally using AngularJS instead of Jquery makes the code very clean keeping only javascript.
    */

    $('.js_slideout_trigger').click(function() {
        id = $(this).data('slideout').split('-')[1];
        t = $(this).data('slideout').split('-')[0];

        $('#slideout-' + id).find('.slideout_type').html(t);

        /*FIRST WAY
       
        if (!($('#slideout-' + id).css('display')!="none" && t !== oldType)){
             $('#slideout-' + id).slideToggle(200);
             
        }*/

        //Second WAY

        if ($('#slideout-' + id).css('display') === "none"){
             
                $('#slideout-' + id).slideDown(200);
                 slideoutViewCount += 1;
                logger.log('slideout:show:' + t);
                logMessageCenter.changeNotify(logger);

        }
        else{
            if(t === oldType){
                $('#slideout-' + id).slideUp(200);   
            }
            else{
                logger.log('slideout:show:' + t);
                logMessageCenter.changeNotify(logger);

            }
        }

     
        oldType = t;
        var n = $(this).closest('li.item').find('h3').text();
        if ($('#js_hotel_history').find('li:contains(' + n + ')').size() === 0) {
            $('#js_hotel_history').prepend('<li>' + n + '</li>');
        }
    });

    $('#js_city_list li').click(function() {
        // Perform new search...

        logger.log('citylist:click');
    });

    $('.querystring_shadow').focus(function() {
        logger.log('searchfield:focus');
    });

    $('.querystring_shadow').keypress(function() {
        logger.log('searchfield:keypress');
    });

    $('#js_short_dealinfos').click(function() {
        logger.log('dealinfo:click');
    });
    $(".single_price").click(function(){
    
        if($(this).find("em").text()==="Cuteowl Inc."){
            $(this).closest("li.item").find(".item_image img").attr("src","images/cbo.jpg");logger.log("cute:owl")
        }
        else if($(this).find("em").text()==="Unicorn Suites"){
            $(this).closest("li.item").find(".item_image img").attr("src","images/u.png");
            logger.log("cute:unicorn")
        }
        else{
            $(this).closest("li.item").find(".item_image img").attr("src","images/hotel_ph.jpg")
        }
    });

})();
